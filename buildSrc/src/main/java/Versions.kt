object Versions {



    //libs
    const val retrofit = "2.9.0"
    const val httpInterceptor = "4.10.0"
    const val kotlinJdk = "1.6.21"
    const val moshi = "1.13.0"
    const val appCompat = "1.4.1"
    const val coreKtx = "1.7.0"
    const val material = "1.6.0"
    const val lifecyleKtx = "2.3.1"
    const val activityCompose = "1.3.1"
    const val roomVersion = "2.4.2"
    const val swipeRefresh = "0.23.1"
    const val systemUIController = "0.17.0"
    const val navAnim = "0.23.0"

    //app
    const val hiltGradleVersion = "2.38.1"
    const val buildVersion = "7.0.4"
    const val kotlinVersion = "1.6.21"
    const val navigationVersion = "2.4.0-alpha02"
    const val composeVersion = "1.2.0-rc02"
    const val pagingVersion = "1.0.0-alpha15"
    const val hiltNavVersion = "1.0.0"
    const val coilVersion = "2.1.0"
    const val hiltVersion = "2.41"



    //test
    const val jUnit = "4.13.2"
    const val courotinesTest = "1.6.1"
    const val testRunner = "1.4.0"
    const val jUnitAndroid = "1.1.3"
    const val espresso = "3.4.0"
    const val arch = "2.1.0"

    const val mockito = "4.0.0"
    const val mockitoKotlin = "3.2.0"


}