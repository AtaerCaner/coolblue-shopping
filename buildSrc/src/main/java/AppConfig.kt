object AppConfig {
    const val applicationId =     "com.coolblueapp"

    const val compileSdk  = 32
    const val minSdk  = 21
    const val targetSdk  = 32
    const val versionCode  = 1
    const val versionName  = "1.0.0"

    const val baseURL = "\"https://bdk0sta2n0.execute-api.eu-west-1.amazonaws.com/mobile-assignment/\""
    const val androidTestInstrumentation = "androidx.test.runner.AndroidJUnitRunner"


}