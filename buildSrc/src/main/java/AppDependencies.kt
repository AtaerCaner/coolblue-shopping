import org.gradle.api.artifacts.dsl.DependencyHandler

object AppDependencies {

    //android ui
    private val appcompat =  "androidx.appcompat:appcompat:${Versions.appCompat}"
    private val coreKtx =  "androidx.core:core-ktx:${Versions.coreKtx}"
    private val material =  "com.google.android.material:material:${Versions.material}"


    //Jetpack
    private val kotlinJdk =  "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlinJdk}"
    private val kotlinMoshi =  "com.squareup.moshi:moshi-kotlin:${Versions.moshi}"
    private val lifecyleKtx =  "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.lifecyleKtx}"
    private val composeUI =  "androidx.compose.ui:ui:${Versions.composeVersion}"
    private val composeMaterial =  "androidx.compose.material:material:${Versions.composeVersion}"
    private val composeCoil =  "io.coil-kt:coil-compose:${Versions.coilVersion}"
    private val composePaging = "androidx.paging:paging-compose:${Versions.pagingVersion}"
    private val composeHilt = "androidx.hilt:hilt-navigation-compose:${Versions.hiltNavVersion}"


    private val toolingPreview =  "androidx.compose.ui:ui-tooling-preview:${Versions.composeVersion}"
    private val toolingPreviewDebug =  "androidx.compose.ui:ui-tooling:${Versions.composeVersion}"
    private val activityCompose =  "androidx.activity:activity-compose:${Versions.activityCompose}"


    //accompanist
    private val accompanistSwipeRefresh =  "com.google.accompanist:accompanist-swiperefresh:${Versions.swipeRefresh}"
    private val accompanistNavAnim =  "com.google.accompanist:accompanist-navigation-animation:${Versions.navAnim}"
    private val accompanistUIController =  "com.google.accompanist:accompanist-systemuicontroller:${Versions.systemUIController}"


    //Network
    private val retrofit =  "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    private val retrofitMoshi =  "com.squareup.retrofit2:converter-moshi:${Versions.retrofit}"
    private val httpInterceptor =  "com.squareup.okhttp3:logging-interceptor:${Versions.httpInterceptor}"

    //Injection
    private val hiltAndroid = "com.google.dagger:hilt-android:${Versions.hiltVersion}"
    private val hiltCompiler = "com.google.dagger:hilt-compiler:${Versions.hiltVersion}"

    //test libs
        // testImplementation
    private val jUnit =  "junit:junit:${Versions.jUnit}"
    private val coroutinesTest =  "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.courotinesTest}"
    private val roomTest =  "androidx.room:room-testing:${Versions.roomVersion}"
    private val mockitoCore =  "org.mockito:mockito-core:${Versions.mockito}"
    private val mockitoKotlin =  "org.mockito.kotlin:mockito-kotlin:${Versions.mockitoKotlin}"
    private val mockitoInline =  "org.mockito:mockito-inline:${Versions.mockito}"
    private val archTesting = "androidx.arch.core:core-testing:${Versions.arch}"



    // androidTestImplementation
    private val testRunner =  "androidx.test:runner:${Versions.testRunner}"
    private val jUnitAndroid =  "androidx.test.ext:junit:${Versions.jUnitAndroid}"
    private val espresso =  "androidx.test.espresso:espresso-core:${Versions.espresso}"
    private val jUnitCompose =  "androidx.compose.ui:ui-test-junit4:${Versions.composeVersion}"
    private val archTestingAndroid =  "androidx.arch.core:core-testing:${Versions.arch}"



    private val androidUILib = mutableListOf<String>().apply {
        add(appcompat)
        add(coreKtx)
        add(material)
    }

    private val jetpackLib = mutableListOf<String>().apply {
        add(kotlinJdk)
        add(kotlinMoshi)
        add(lifecyleKtx)
        add(composeUI)
        add(composeMaterial)
        add(composeCoil)
        add(composePaging)
        add(composeHilt)
        add(toolingPreview)
        add(toolingPreviewDebug)
        add(activityCompose)
        add(accompanistSwipeRefresh)
        add(accompanistNavAnim)
        add(accompanistUIController)
    }

    private val networkLib = mutableListOf<String>().apply {
        add(retrofit)
        add(retrofitMoshi)
        add(httpInterceptor)
    }

    private val injectionLib = mutableListOf<String>().apply {
        add(hiltAndroid)
    }

    val appLibraries = mutableListOf<String>().apply {
        addAll(androidUILib)
        addAll(jetpackLib)
        addAll(networkLib)
        addAll(injectionLib)
    }

    val kaptLibraries = mutableListOf<String>().apply {
        add(hiltCompiler)
    }

    val testLibraries = mutableListOf<String>().apply {
        add(jUnit)
        add(coroutinesTest)
        add(roomTest)
        add(mockitoCore)
        add(mockitoKotlin)
        add(mockitoInline)
        add(archTesting)
    }

    val androidTestLibraries = mutableListOf<String>().apply {
        add(testRunner)
        add(jUnitAndroid)
        add(jUnitCompose)
        add(espresso)
        add(archTestingAndroid)
    }

    val debugLibraries = mutableListOf<String>().apply {
        add(toolingPreviewDebug)
    }



    fun DependencyHandler.kapt(list: List<String>) {
        list.forEach { dependency ->
            add("kapt", dependency)
        }
    }

    fun DependencyHandler.implementation(list: List<String>) {
        list.forEach { dependency ->
            add("implementation", dependency)
        }
    }

    fun DependencyHandler.androidTestImplementation(list: List<String>) {
        list.forEach { dependency ->
            add("androidTestImplementation", dependency)
        }
    }

    fun DependencyHandler.testImplementation(list: List<String>) {
        list.forEach { dependency ->
            add("testImplementation", dependency)
        }
    }
}