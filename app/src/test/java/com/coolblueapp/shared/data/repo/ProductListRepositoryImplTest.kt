package com.coolblueapp.shared.data.repo

import androidx.paging.PagingSource
import com.coolblueapp.BaseUnitTest
import com.coolblueapp.shared.data.network.CoolblueService
import com.coolblueapp.shared.data.repo.source.ProductSource
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.kotlin.*

class ProductListRepositoryImplTest : BaseUnitTest() {


    @Mock
    private lateinit var apiSer: CoolblueService

    lateinit var productListRepo: ProductListRepository

    lateinit var productSource: ProductSource

    @Before
    fun setup() {
        productListRepo = ProductListRepositoryImpl(apiService = apiSer)
        productSource = ProductSource(productListRepo, ""){}
    }

    @Test
    fun testProductListApi() = runBlockingTest {
        val pageNumber = 1
        val query = "apple"

        apiSer.getProductList(pageNumber = pageNumber, query = query)

        verify(apiSer).getProductList(pageNumber = pageNumber, query = query)

    }

    @Test
    fun testProductListRepoImpl() = runBlockingTest {
        val pageNumber = 1
        val query = "apple"

        productListRepo.getProductList(pageNumber = pageNumber, query = query)

        verify(apiSer).getProductList(pageNumber = pageNumber, query = query)

    }



    @Test
    fun testProductSource() = runBlockingTest {
        val params: PagingSource.LoadParams<Int> = mock()

        whenever(
            apiSer.getProductList(
                anyVararg(),
                anyVararg()
            )
        ).thenReturn(mock())

        productSource.load(params)

        verify(apiSer).getProductList(
            anyVararg(),
            anyVararg()
        )
    }


}