package com.coolblueapp

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.coolblueapp.di.CoroutineRule
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Rule
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
abstract class BaseUnitTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val coroutineRule = CoroutineRule()

    fun runBlockingTest(block: suspend TestCoroutineScope.() -> Unit) =
        coroutineRule.testDispatcher.runBlockingTest { block() }
}