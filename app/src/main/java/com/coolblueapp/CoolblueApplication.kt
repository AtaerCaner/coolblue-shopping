package com.coolblueapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CoolblueApplication:Application() {
}