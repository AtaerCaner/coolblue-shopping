package com.coolblueapp.di

import com.coolblueapp.shared.data.repo.ProductListRepository
import com.coolblueapp.shared.data.repo.ProductListRepositoryImpl
import com.coolblueapp.shared.data.repo.source.ProductSource
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
abstract class RepositoryModule {

    @Binds
    abstract fun provideProductListRepository(productListRepositoryImpl: ProductListRepositoryImpl
    ): ProductListRepository


}