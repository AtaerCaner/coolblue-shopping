package com.coolblueapp.shared.data.network.model


import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Product(
    @Json(name= "availabilityState")
    val availabilityState: Int,
    @Json(name= "coolbluesChoiceInformationTitle")
    val coolbluesChoiceInformationTitle: String?,
    @Json(name= "listPriceExVat")
    val listPriceExVat: Double?,
    @Json(name= "listPriceIncVat")
    val listPriceIncVat: Int?,
    @Json(name= "nextDayDelivery")
    val nextDayDelivery: Boolean,
    @Json(name= "productId")
    val productId: Int,
    @Json(name= "productImage")
    val productImage: String?,
    @Json(name= "productName")
    val productName: String?,
    @Json(name= "promoIcon")
    val promoIcon: PromoIcon?,
    @Json(name= "reviewInformation")
    val reviewInformation: ReviewInformation,
    @Json(name= "salesPriceIncVat")
    val salesPriceIncVat: Double?,
    @Json(name= "USPs")
    val uSPs: List<String>?
): Parcelable