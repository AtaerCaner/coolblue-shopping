package com.coolblueapp.shared.data.repo

import com.coolblueapp.shared.data.network.model.ProductListResponse
import kotlinx.coroutines.flow.Flow

interface ProductListRepository {
    suspend fun getProductList(pageNumber: Int,query: String): ProductListResponse
}