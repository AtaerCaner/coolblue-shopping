package com.coolblueapp.shared.data.network

import com.coolblueapp.shared.data.network.model.ProductListResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface CoolblueService {
    @GET("search")
    suspend fun getProductList(
        @Query("page") pageNumber: Int,
        @Query("query") query: String
    ): ProductListResponse
}