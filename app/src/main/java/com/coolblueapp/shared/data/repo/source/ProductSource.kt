package com.coolblueapp.shared.data.repo.source

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.coolblueapp.shared.data.network.model.Product
import com.coolblueapp.shared.data.network.model.ProductListResponse
import com.coolblueapp.shared.data.repo.ProductListRepository
import okio.IOException
import retrofit2.HttpException
import javax.inject.Inject

class ProductSource(
    private val productListRepository: ProductListRepository,
    private val query: String,
    private val onResponse: (Boolean) -> Unit
) :
    PagingSource<Int, Product>() {


    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Product> {
        return try {
            val nextPage = params.key ?: 1

            val productListResponse: ProductListResponse =
                productListRepository.getProductList(pageNumber = nextPage, query = query)

            onResponse(nextPage == 1)

            val prevKey = if (nextPage == 1) null else nextPage - 1

            val nextKey =
                if (productListResponse.pageCount != productListResponse.currentPage)
                    productListResponse.currentPage?.plus(1)
                else
                    null

            LoadResult.Page(
                data = productListResponse.products!!,
                prevKey = prevKey,
                nextKey = nextKey
            )
        } catch (exception: IOException) {
            return LoadResult.Error(exception)
        } catch (exception: HttpException) {
            return LoadResult.Error(exception)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, Product>): Int? = null
}