package com.coolblueapp.shared.data.network.model


import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PromoIcon(
    @Json(name="text")
    val text: String?,
    @Json(name="type")
    val type: String?
): Parcelable