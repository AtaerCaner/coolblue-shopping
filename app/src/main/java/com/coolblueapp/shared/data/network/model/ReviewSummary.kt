package com.coolblueapp.shared.data.network.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ReviewSummary(
    @Json(name = "reviewAverage")
    val reviewAverage: Double,
    @Json(name = "reviewCount")
    val reviewCount: Int?
): Parcelable