package com.coolblueapp.shared.data.network.model


import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProductListResponse(
    @Json(name= "currentPage")
    val currentPage: Int?,
    @Json(name= "pageCount")
    val pageCount: Int?,
    @Json(name= "pageSize")
    val pageSize: Int?,
    @Json(name= "products")
    val products: List<Product>?,
    @Json(name= "totalResults")
    val totalResults: Int?
):Parcelable