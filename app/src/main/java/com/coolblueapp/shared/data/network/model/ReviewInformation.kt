package com.coolblueapp.shared.data.network.model


import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ReviewInformation(
    @Json(name = "reviewSummary")
    val reviewSummary: ReviewSummary?
): Parcelable