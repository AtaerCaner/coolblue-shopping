package com.coolblueapp.shared.data.repo

import com.coolblueapp.shared.data.network.CoolblueService
import com.coolblueapp.shared.data.network.model.ProductListResponse
import javax.inject.Inject

class ProductListRepositoryImpl @Inject constructor(private val apiService: CoolblueService):ProductListRepository {
    override suspend fun getProductList(pageNumber: Int, query: String): ProductListResponse =
        apiService.getProductList(pageNumber = pageNumber, query = query)

}