package com.coolblueapp.presentation.ui

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import com.coolblueapp.presentation.navigation.NavigationComponent
import com.coolblueapp.presentation.theme.CoolblueAppTheme
import com.google.accompanist.navigation.animation.rememberAnimatedNavController
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val navController = rememberAnimatedNavController()
            CoolblueAppTheme {
                NavigationComponent(navController = navController)
            }
        }
    }
}