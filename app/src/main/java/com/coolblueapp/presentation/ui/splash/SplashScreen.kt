package com.coolblueapp.presentation.ui.splash

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.coolblueapp.R
import com.coolblueapp.presentation.navigation.Screen
import com.coolblueapp.presentation.theme.Blue100
import com.coolblueapp.presentation.theme.Blue200
import kotlinx.coroutines.delay


private val SPLASH_DELAY_TIME = 1500L

@Composable
fun SplashScreen(navController: NavHostController) {
    Box(
        Modifier
            .fillMaxSize()
            .background(Blue100)
    ) {
        Image(
            painter = painterResource(id = R.drawable.cb_logo),
            contentDescription = "",
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .size(80.dp)
                .align(Alignment.Center)
        )
    }

    LaunchedEffect(true) {
        delay(SPLASH_DELAY_TIME)
        navigateToList(navController)
    }
}

private fun navigateToList(navController: NavHostController) {
    navController.navigate(
        route = Screen.List.route,
        builder = {
            popUpTo(Screen.Splash.route) {
                inclusive = true
            }
        }
    )


}