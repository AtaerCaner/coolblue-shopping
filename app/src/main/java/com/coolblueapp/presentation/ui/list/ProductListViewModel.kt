package com.coolblueapp.presentation.ui.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.coolblueapp.shared.data.repo.ProductListRepository
import com.coolblueapp.shared.data.repo.source.ProductSource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class ProductListViewModel @Inject constructor(private val productListRepository: ProductListRepository) :
    ViewModel() {
    private val _searchQuery = MutableStateFlow("Apple")
    val searchQuery: StateFlow<String> = _searchQuery

    val fullyRefresh = MutableStateFlow(true)

    val producListFlow = searchQuery
        .debounce(1000)
        .distinctUntilChanged()
        .filter { it.length > 2 }
        .flatMapLatest {
            Pager(config = PagingConfig(pageSize = 10)) {

                ProductSource(
                    query = it,
                    productListRepository = productListRepository
                ) { isReload->
                    if (isReload) {
                        fullyRefresh.value = true
                    }
                }

            }.flow
                .onStart {
                    emit(PagingData.empty())
                }
                .cachedIn(viewModelScope)
        }


    fun onQueryChange(it: String) {
        _searchQuery.value = it
    }

}