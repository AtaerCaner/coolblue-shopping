package com.coolblueapp.presentation.ui.list

import android.annotation.SuppressLint
import android.util.Log
import android.view.MotionEvent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterEnd
import androidx.compose.ui.Alignment.Companion.CenterStart
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInteropFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalTextInputService
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.paging.LoadState
import androidx.paging.PagingData
import com.coolblueapp.shared.data.network.model.Product
import kotlinx.coroutines.flow.Flow
import androidx.paging.compose.collectAsLazyPagingItems
import androidx.paging.compose.items
import coil.compose.rememberAsyncImagePainter
import com.coolblueapp.presentation.common.ErrorItem
import com.coolblueapp.presentation.common.LoadingItem
import com.coolblueapp.presentation.common.LoadingView
import com.coolblueapp.shared.data.network.model.ReviewSummary
import com.coolblueapp.presentation.theme.Blue200
import com.coolblueapp.R
import com.coolblueapp.presentation.common.NoPaddingTextField
import com.coolblueapp.presentation.theme.Blue50
import com.coolblueapp.presentation.theme.Blue100
import com.coolblueapp.presentation.theme.GreenCB
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

@Composable
fun ProductListScreen(
    onAddCardClicked: (Int) -> Unit,
    onQueryChange: (String) -> Unit,
    query: String,
    productList: Flow<PagingData<Product>>,
    fullyRefresh: MutableStateFlow<Boolean>
) {

    Column {
        TopSearchBar(query = query, onQueryChange = onQueryChange)
        ProductList(
            productList = productList,
            onAddCardClicked = onAddCardClicked,
            fullyRefresh = fullyRefresh
        )
    }
}

@Composable
fun TopSearchBar(query: String, onQueryChange: (String) -> Unit) {
    val inputService = LocalTextInputService.current

    Column(
        Modifier
            .background(Blue100)
            .padding(vertical = 10.dp, horizontal = 20.dp)
    ) {
        Surface(
            color = Color.White,
            shape = RoundedCornerShape(3.dp)
        ) {

            Row(Modifier.padding(all = 8.dp), verticalAlignment = Alignment.CenterVertically) {
                Icon(
                    painter = painterResource(R.drawable.product_search),
                    modifier = Modifier.size(16.dp),
                    contentDescription = "",
                    tint = Blue100,
                )



                NoPaddingTextField(
                    placeholder = "What are you looking for?",
                    value = query,
                    onValueChange = { onQueryChange(it) },
                    modifier = Modifier
                        .padding(start = 10.dp)
                        .fillMaxWidth(),
                    keyboardOptions = KeyboardOptions.Default.copy(
                        imeAction = ImeAction.Done,
                        keyboardType = KeyboardType.Text
                    ),
                    keyboardActions = KeyboardActions(onDone = { inputService?.hideSoftwareKeyboard() })
                )

            }
        }
    }
}


@SuppressLint("StateFlowValueCalledInComposition", "CoroutineCreationDuringComposition")
@Composable
private fun ProductList(
    onAddCardClicked: (Int) -> Unit,
    productList: Flow<PagingData<Product>>,
    fullyRefresh: MutableStateFlow<Boolean>
) {
    val lazyProductList = productList.collectAsLazyPagingItems()
    val listState = rememberLazyListState()
    val coroutineScope = rememberCoroutineScope()
    val inputService = LocalTextInputService.current

    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
            .pointerInteropFilter {
                if (it.action == MotionEvent.ACTION_DOWN) {
                    inputService?.hideSoftwareKeyboard()
                }
                false
            },
        state = listState
    ) {
        items(lazyProductList) {
            ProductItem(product = it, onAddCardClicked = onAddCardClicked)
        }

        lazyProductList.apply {
            when {
                loadState.refresh is LoadState.Loading -> {
                    item { LoadingView(Modifier.fillParentMaxSize()) }
                }
                loadState.refresh is LoadState.Error -> {

                    val e = lazyProductList.loadState.refresh as LoadState.Error
                    item {
                        ErrorItem(message = e.error.localizedMessage!!) { }
                    }
                }

                loadState.append is LoadState.Loading -> {
                    item { LoadingItem() }
                }

                loadState.append is LoadState.Error -> {
                    val e = lazyProductList.loadState.append as LoadState.Error
                    item {
                        ErrorItem(message = e.error.localizedMessage!!) { }
                    }
                }


            }
        }
    }
    if (fullyRefresh.collectAsState().value) {
        coroutineScope.launch {
            listState.scrollToItem(0, 0)
        }
        fullyRefresh.value = false
    }

}

@Composable
private fun ProductItem(product: Product?, onAddCardClicked: (Int) -> Unit) {
    product ?: return

    Column {
        Row(
            Modifier
                .fillMaxWidth()
                .wrapContentHeight()
                .background(Color.White)
                .padding(vertical = 8.dp, horizontal = 10.dp)
        ) {

            Image(
                painter = rememberAsyncImagePainter(model = product.productImage),
                contentDescription = "",
                contentScale = ContentScale.Inside,
                modifier = Modifier
                    .width(100.dp)
                    .height(150.dp)
            )

            Column(Modifier.padding(start = 8.dp)) {

                //product title
                Text(
                    text = product.productName!!,
                    fontSize = 15.sp,
                    color = Blue50,
                    fontWeight = FontWeight.Bold
                )

                ReviewRow(product.reviewInformation.reviewSummary)

                USPColumn(product.uSPs)

                //product price
                Text(
                    text = "${product.salesPriceIncVat}",
                    color = Color.Black,
                    fontWeight = FontWeight.Bold,
                    fontSize = 15.sp,
                    modifier = Modifier.padding(top = 2.dp)
                )

                DeliveryRow(
                    nextDayDelivery = product.nextDayDelivery,
                    availabilityState = product.availabilityState,
                    onAddCardClicked = {
                        onAddCardClicked(product.productId)
                    }
                )

            }

        }

        Divider(
            Modifier
                .fillMaxWidth()
                .height(1.dp), color = Color.LightGray
        )
    }
}

@Composable
fun ReviewRow(reviewSummary: ReviewSummary?) {
    reviewSummary ?: return
    val rvCount = reviewSummary.reviewAverage.toInt() / 2
    Row(
        modifier = Modifier.padding(bottom = 6.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        for (i in 0 until rvCount) {
            Icon(
                painter = painterResource(R.drawable.star_full),
                modifier = Modifier.size(16.dp),
                contentDescription = "",
                tint = GreenCB,
            )
        }
        if (reviewSummary.reviewAverage % 2 != 0.0) {
            Icon(
                painter = painterResource(R.drawable.star_half_empty),
                modifier = Modifier.size(16.dp),
                contentDescription = "",
                tint = GreenCB,
            )
        }

        Text(
            text = "${reviewSummary.reviewCount} reviews",
            color = Blue200,
            fontSize = 14.sp,
            modifier = Modifier.padding(start = 6.dp)
        )
    }
}

@Composable
fun USPColumn(uSPs: List<String>?) {
    uSPs ?: return
    Column {
        uSPs.forEach {
            Row(Modifier.padding(bottom = 2.dp), verticalAlignment = Alignment.Top) {
                Text(text = "•", color = Blue200, fontSize = 16.sp)
                Text(
                    text = it,
                    color = Blue200,
                    fontSize = 14.sp,
                    modifier = Modifier.padding(start = 4.dp)
                )
            }
        }
    }
}


@Composable
fun DeliveryRow(nextDayDelivery: Boolean, availabilityState: Int, onAddCardClicked: () -> Unit) {
    Box(
        Modifier
            .padding(top = 10.dp)
            .fillMaxWidth()
    ) {
        Column(Modifier.align(CenterStart)) {
            if (nextDayDelivery) {
                Row(verticalAlignment = Alignment.CenterVertically) {
                    Image(
                        painter = painterResource(id = R.drawable.circle_check),
                        contentDescription = "",
                        contentScale = ContentScale.Inside,
                        modifier = Modifier
                            .size(15.dp)
                    )
                    Text(
                        text = "Delivered Tomorrow",
                        color = GreenCB,
                        fontWeight = FontWeight.Bold,
                        fontSize = 13.sp,
                        modifier = Modifier.padding(start = 6.dp)
                    )
                }
            }

            Text(
                text = "Available for pickup in $availabilityState stores",
                color = Color.Gray,
                fontSize = 12.sp,
                modifier = Modifier.padding(top = 2.dp)
            )
        }

        Surface(
            modifier = Modifier
                .align(CenterEnd)
                .clickable { onAddCardClicked() },
            shape = RoundedCornerShape(5.dp),
            color = GreenCB
        ) {
            Icon(
                painter = painterResource(id = R.drawable.add_card_btn),
                contentDescription = "",
                modifier = Modifier
                    .padding(all = 10.dp)
                    .height(20.dp)
                    .width(30.dp),
                tint = Color.White

            )
        }

    }
}




