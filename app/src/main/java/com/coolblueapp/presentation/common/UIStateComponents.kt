package com.coolblueapp.presentation.common

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.DialogProperties
import com.coolblueapp.presentation.theme.Blue100

@Composable
fun LoadingView(modifier: Modifier = Modifier) {
    Column(
        modifier = modifier.background(Color.White),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        CircularProgressIndicator(color = Blue100)
    }
}

@Composable
fun LoadingItem() {
    CircularProgressIndicator(
        modifier = Modifier
            .fillMaxWidth()
            .background(Color.White)
            .padding(16.dp)
            .wrapContentWidth(Alignment.CenterHorizontally),
        color = Blue100
    )
}

@Composable
fun ErrorItem(
    message: String,
    onRetry: () -> Unit
) {
    val dialogDisplaying = remember { mutableStateOf(true) }
    if (dialogDisplaying.value) {
        AlertDialog(
            backgroundColor = Color.White,
            onDismissRequest = {
                dialogDisplaying.value = false
                onRetry()
            },
            properties = DialogProperties(
                dismissOnBackPress = false,
                dismissOnClickOutside = false
            ),
            confirmButton = {
                Text(
                    text = "OK",
                    fontSize = 13.sp,
                    color = Blue100,
                    modifier = Modifier.padding(all = 5.dp).clickable {
                        dialogDisplaying.value = false
                        onRetry()
                    }
                )
            },
            text = {
                Text(
                    text = "Something went wrong in. Try again later \n message=$message",
                    fontSize = 13.sp,
                    color = Color.Black
                )
            })
    }

}