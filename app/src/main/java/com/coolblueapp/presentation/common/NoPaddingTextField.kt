package com.coolblueapp.presentation.common

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.sp

@Composable
fun NoPaddingTextField(
    placeholder: String? = "",
    value: String,
    onValueChange: (String) -> Unit,
    modifier: Modifier? = Modifier.fillMaxWidth(),
    keyboardOptions: KeyboardOptions? = KeyboardOptions.Default,
    keyboardActions: KeyboardActions? = KeyboardActions { },
) {
    BasicTextField(
        modifier = modifier!!,
        value = value,
        onValueChange = onValueChange,
        keyboardActions = keyboardActions!!,
        keyboardOptions = keyboardOptions!!,
        textStyle = TextStyle(
            color = Color.Gray
        ),
        decorationBox = { innerTextField ->
            Column(modifier = Modifier.fillMaxWidth()) {
                if (value.isEmpty() && !placeholder.isNullOrEmpty()) {
                    Text(
                        text = placeholder,
                        color = Color.LightGray,
                        fontSize = 14.sp
                    )
                }
            }
            Column {
                innerTextField()
            }
        }
    )
}