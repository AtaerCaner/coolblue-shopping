package com.coolblueapp.presentation.navigation

sealed class Screen(val route: String) {
    object Splash : Screen("splash_screen")
    object List : Screen("list_screen")
}