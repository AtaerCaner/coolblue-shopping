package com.coolblueapp.presentation.navigation

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.coolblueapp.presentation.ui.list.ProductListScreen
import com.coolblueapp.presentation.ui.list.ProductListViewModel
import com.google.accompanist.navigation.animation.composable
import com.coolblueapp.presentation.ui.splash.SplashScreen
import com.google.accompanist.navigation.animation.AnimatedNavHost

@Composable
fun NavigationComponent(navController: NavHostController) {
    AnimatedNavHost(
        navController = navController,
        startDestination = Screen.Splash.route
    ) {
        composable(route = Screen.Splash.route) {
            SplashScreen(navController = navController)
        }
        composable(route = Screen.List.route) {
            val viewModel = hiltViewModel<ProductListViewModel>()
            ProductListScreen(
                productList = viewModel.producListFlow,
                query = viewModel.searchQuery.collectAsState().value,
                onQueryChange = {
                    viewModel.onQueryChange(it)
                },
                fullyRefresh = viewModel.fullyRefresh,
                onAddCardClicked = { productId ->
                    //AddCard action can handle in this scope
                }
            )

        }
    }
}
