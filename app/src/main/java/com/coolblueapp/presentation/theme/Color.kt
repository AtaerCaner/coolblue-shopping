package com.coolblueapp.presentation.theme


import androidx.compose.ui.graphics.Color
import com.coolblueapp.shared.ext.color

val Blue50 = "#63A9E1".color
val Blue100 = "#3F8EDD".color
val Blue200 = "#14181E".color
val Blue500 = "#11161C".color
val Orange200 = "#ED702D".color
val GreenCB = "#53B635".color