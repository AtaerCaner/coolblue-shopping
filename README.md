# About Coolblue Shopping App

Coolblue Shopping App is a sample project to use the latest ``Android Architecture Components`` and ``MVVM`` design patterns. App uses single activity and multiple Compose Screens as UI hierarchy. It has two screens such as Splash and Product List

1. **Splash:** Navigates to Product List with using Compose Navigation Framework

2. **Product List:** Screen initialize with default query which is "Apple". More product appends to list by scrolling and when the query changes, list reloads with new products and scrolls to top after 1000 milisecond debounce.


## Decisions and Important Notes
- I used **buildSrc** for the dependencies as Kotlin DSL files

- Since I use **Compose Navigation Architecture** with single container activity, I didn't use a *BaseActivity*. If standalone flows like Login/Register will add to project, that flows can implement with another activity and then a *BaseActivity* can create

- I didn't use *ResultWrapper* because *Paging* library has *LoadResult* for wrapping response to handle error, loading and success states. It emits response as *flow*.

- I used **ProductSource** as an *UseCase*. **ProductSource** reaches to repository, fetches response from API and transforms it. For another business flows that will implement in future, usecases can be use.
